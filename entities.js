"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Player = exports.Rocket = exports.Alien = exports.Laser = exports.Meteor = void 0;
var PIXI = require("pixi.js");
var constantes_1 = require("./constantes");
var Sprite = PIXI.Sprite;
var ExperimentProjectilFactory = /** @class */ (function () {
    function ExperimentProjectilFactory(texture) {
        this.texture = texture;
        this.missleSound = document.createElement("audio");
        document.body.appendChild(this.missleSound);
        this.missleSound.setAttribute("src", "sounds/rocket.wav");
        this.laserSound = document.createElement("audio");
        document.body.appendChild(this.laserSound);
        this.laserSound.setAttribute("src", "sounds/laser.wav");
    }
    ExperimentProjectilFactory.prototype.buildRocket = function (x, y, width, height) {
        this.missleSound.play();
        return new Rocket(this.texture, x, y, width, height);
    };
    ExperimentProjectilFactory.prototype.buildLaser = function (x, y, width, height) {
        this.laserSound.play();
        return new Laser(this.texture, x, y, width, height);
    };
    return ExperimentProjectilFactory;
}());
var Meteor = /** @class */ (function (_super) {
    __extends(Meteor, _super);
    function Meteor(texture, x, y, width, height) {
        var _this = _super.call(this, texture) || this;
        _this.position.x = x;
        _this.position.y = y;
        _this.width = width;
        _this.height = height;
        _this.pivot.set(_this.width / 2, _this.height / 2);
        return _this;
    }
    Meteor.prototype.update = function () {
        this.rotation += 0.01;
        this.position.y += 1.2;
    };
    return Meteor;
}(Sprite));
exports.Meteor = Meteor;
var Laser = /** @class */ (function (_super) {
    __extends(Laser, _super);
    function Laser(texture, x, y, width, height) {
        var _this = _super.call(this, texture) || this;
        _this.position.x = x;
        _this.position.y = y;
        _this.width = width;
        _this.height = height;
        _this.velocity = 1.10;
        return _this;
    }
    Laser.prototype.update = function () {
        this.y += this.velocity;
    };
    return Laser;
}(Sprite));
exports.Laser = Laser;
var Alien = /** @class */ (function (_super) {
    __extends(Alien, _super);
    function Alien(texture, textureLaser, x, y, width, height) {
        var _this = _super.call(this, texture) || this;
        _this.buildLaser = new ExperimentProjectilFactory(textureLaser);
        _this.position.x = x;
        _this.position.y = y;
        _this.width = width;
        _this.height = height;
        _this.projectils = [];
        _this.time = 0;
        _this.velocity = 1.3;
        return _this;
    }
    Alien.prototype.update = function () {
        this.position.y += this.velocity;
    };
    Alien.prototype.updateFire = function () {
        this.time++;
        if (this.time == 150) {
            this.projectils.push(this.buildLaser.buildLaser(this.x + 22, this.y + 20, 11, 26));
            this.time = 0;
        }
    };
    return Alien;
}(Sprite));
exports.Alien = Alien;
var Rocket = /** @class */ (function (_super) {
    __extends(Rocket, _super);
    function Rocket(texture, x, y, width, height) {
        var _this = _super.call(this, texture) || this;
        _this.position.x = x;
        _this.position.y = y;
        _this.width = width;
        _this.height = height;
        _this.velocity = 0.10;
        _this.aceleration = 1.1;
        _this.friction = 0.96;
        _this.speedLimit = 5;
        return _this;
    }
    Rocket.prototype.update = function () {
        this.velocity = Math.min(this.speedLimit, Math.max(this.velocity, -this.speedLimit));
        this.velocity *= this.aceleration;
        this.velocity *= this.friction;
        this.position.y -= this.velocity;
    };
    return Rocket;
}(Sprite));
exports.Rocket = Rocket;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player(texture, textureRocket, x, y, width, height) {
        var _this = _super.call(this, texture) || this;
        _this.position.x = x;
        _this.position.y = y;
        _this.width = width;
        _this.height = height;
        _this.keyState = { 32: false, 37: false, 38: false, 39: false, 40: false };
        _this.keyCodes = { 37: -1, 38: -1, 39: 1, 40: 1 };
        _this.directionX = 0;
        _this.directionY = 0;
        _this.accelerationX = 0;
        _this.accelerationY = 0;
        _this.velocityX = 0;
        _this.velocityY = 0;
        _this.friction = 0.96;
        _this.speed = 10;
        _this.fireSpeed = 40;
        _this.speedLimit = 5;
        _this.fireCooldown = 0;
        _this.projectils = [[], []];
        _this.buildRocket = new ExperimentProjectilFactory(textureRocket);
        _this.widthRocket = 20;
        _this.heightRocket = 44;
        window.addEventListener('keydown', _this.onKeyDown.bind(_this));
        window.addEventListener('keyup', _this.onKeyUp.bind(_this));
        return _this;
    }
    Player.prototype.update = function () {
        this.velocityX += this.directionX * this.accelerationX;
        this.velocityY += this.directionY * this.accelerationY;
        //Apply friction
        this.velocityX *= this.friction;
        this.velocityY *= this.friction;
        this.velocityX = Math.min(this.speedLimit, Math.max(this.velocityX, -this.speedLimit));
        this.velocityY = Math.min(this.speedLimit, Math.max(this.velocityY, -this.speedLimit));
        if ((this.y + this.height) > constantes_1.HEIGHT) {
            this.y = constantes_1.HEIGHT - this.height;
        }
        else if (this.x < 0 - this.width) {
            this.x = constantes_1.WIDTH - this.width;
        }
        else if ((this.x + this.width / 2) > constantes_1.WIDTH) {
            this.x = 0 + this.width;
        }
        else if ((this.y - this.height) < (constantes_1.HEIGHT / 3)) {
            this.y = constantes_1.HEIGHT / 3 + this.height;
        }
        else {
            this.x += this.velocityX;
            this.y += this.velocityY;
        }
        this.updateFire();
    };
    Player.prototype.updateFire = function () {
        if (this.shoot) {
            if (this.fireCooldown < this.fireSpeed)
                this.fireCooldown++;
        }
        if (this.fireCooldown >= this.fireSpeed) {
            this.projectils[0].push(this.buildRocket.buildRocket(this.position.x, this.position.y, this.widthRocket, this.heightRocket));
            this.projectils[1].push(this.buildRocket.buildRocket(this.position.x + this.width - 20, this.position.y, this.widthRocket, this.heightRocket));
            this.fireCooldown = 0;
            this.shoot = false;
        }
    };
    Player.prototype.onKeyDown = function (key) {
        this.keyState[key.keyCode] = true;
        if (key.keyCode == 37 || key.keyCode == 39) {
            this.directionX = this.keyCodes[key.keyCode];
            this.accelerationX = 0.3;
        }
        else if (key.keyCode == 38 || key.keyCode == 40) {
            this.directionY = this.keyCodes[key.keyCode];
            this.accelerationY = 0.3;
        }
        else if (key.keyCode == 32) {
            this.shoot = true;
        }
    };
    Player.prototype.onKeyUp = function (key) {
        this.keyState[key.keyCode] = false;
        if (!this.keyState[37] && this.keyState[39])
            this.directionX = this.keyCodes[39];
        else if (this.keyState[37] && !this.keyState[39])
            this.directionX = this.keyCodes[37];
        else
            this.directionX = 0, this.accelerationX = 0, this.friction = 0.96;
        if (!this.keyState[38] && this.keyState[40])
            this.directionY = this.keyCodes[40];
        else if (this.keyState[38] && !this.keyState[40])
            this.directionY = this.keyCodes[38];
        else
            this.directionY = 0, this.accelerationY = 0, this.friction = 0.96;
    };
    return Player;
}(Sprite));
exports.Player = Player;

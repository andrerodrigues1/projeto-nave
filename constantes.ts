import {remote} from 'electron';

import PIXI = require('pixi.js');
const loader = new PIXI.Loader();
var Texture =  PIXI.Texture;
var Rectangle = PIXI.Rectangle;

 export const WIDTH = 600, HEIGHT = 360; 

 export enum TypeEntity {
    PLAYER, ENEMY, BACKGROUD, ROCKET, METEOR, EXPLOSION
}

 export const configureExplosion = (sheet : PIXI.BaseTexture) => { 
 
     let matrix =  [
        new Texture(sheet, new Rectangle(0,0,128,128)),
        new Texture(sheet, new Rectangle(128,0,128,128)),
        new Texture(sheet, new Rectangle(256,0,128,128)),
        new Texture(sheet, new Rectangle(384, 0, 128, 128)),
        new Texture(sheet, new Rectangle(0,128,128,128)),
        new Texture(sheet, new Rectangle(128,128,128,128)),
        new Texture(sheet, new Rectangle(256,128,128,128)),
        new Texture(sheet, new Rectangle(384,128,128,128)),
        new Texture(sheet, new Rectangle(0, 256, 128, 128)),
        new Texture(sheet, new Rectangle(128, 256, 128, 128)),
        new Texture(sheet, new Rectangle(256, 256, 128, 128)),
        new Texture(sheet, new Rectangle(384, 256, 128, 128)),
        new Texture(sheet, new Rectangle(0, 384, 128, 128)),
        new Texture(sheet, new Rectangle(128, 384, 128, 128)),
        new Texture(sheet, new Rectangle(256, 384, 128, 128)),
        new Texture(sheet, new Rectangle(384, 384, 128, 128))
 ];
    return matrix;
}

 

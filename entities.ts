import {remote} from 'electron';

import PIXI = require('pixi.js');

import {WIDTH, HEIGHT} from './constantes';

var Sprite = PIXI.Sprite;

interface Entity {
    update() : void;
}

interface Attack {
    updateFire() : void;
}

interface ProjectilFactory {
    buildRocket(x:number, y:number,  width:number, height:number) : Rocket;
    buildLaser(x:number, y:number,  width:number, height:number) : Laser;
}

class ExperimentProjectilFactory implements ProjectilFactory {
    texture : PIXI.Texture;
    missleSound : HTMLAudioElement;
    laserSound : HTMLAudioElement;

    constructor( texture : PIXI.Texture) {
        this.texture = texture;
        this.missleSound = document.createElement("audio");
		document.body.appendChild(this.missleSound);
        this.missleSound.setAttribute("src","sounds/rocket.wav");
        
        this.laserSound = document.createElement("audio");
		document.body.appendChild(this.laserSound);
		this.laserSound.setAttribute("src","sounds/laser.wav");
    }

    buildRocket(x:number, y:number,  width:number, height:number) : Rocket {
        this.missleSound.play();
        return new Rocket(this.texture, x, y, width, height);
    }
    
    buildLaser(x:number, y:number,  width:number, height:number) : Laser {
        this.laserSound.play();
        return new Laser(this.texture, x, y, width, height);
    }
}  

export class Meteor extends Sprite implements Entity { 
    constructor(texture : PIXI.Texture, x:number, y:number,  width:number, height:number) {
      super(texture);
      this.position.x = x;
      this.position.y = y;
      this.width = width;
      this.height = height;
      this.pivot.set(this.width/2, this.height/2);       
    }

    update() {
        this.rotation += 0.01; 
        this.position.y+= 1.2;
    }
}   

export class Laser extends Sprite implements Entity {
    velocity : number;
    constructor(texture : PIXI.Texture, x:number, y:number,  width:number, height:number) {
        super(texture);
        this.position.x = x;
        this.position.y = y;
        this.width = width;
        this.height = height;
        this.velocity = 1.10;
    }

    update() {
        this.y += this.velocity;
    }
}

export class Alien extends Sprite implements Entity, Attack {
    time : number;
    projectils : Laser[]; 
    velocity : number;
    buildLaser: ExperimentProjectilFactory;
    constructor(texture : PIXI.Texture, textureLaser : PIXI.Texture, x : number, y : number, width : number, height : number) {
        super(texture)
        this.buildLaser = new ExperimentProjectilFactory(textureLaser);
        this.position.x = x;
        this.position.y = y;
        this.width = width;
        this.height = height;
        this.projectils = [];
        this.time = 0;
        this.velocity = 1.3;
    }

    update() {
        this.position.y+= this.velocity;
    }

    updateFire() {
        this.time++;
    
        if(this.time == 150){
             this.projectils.push(this.buildLaser.buildLaser(this.x + 22, this.y + 20, 11, 26));
            
            this.time = 0;
        }
    }
}

export class Rocket extends Sprite implements Entity {
    velocity : number;
    aceleration : number;
    friction : number;
    speedLimit : number;	
  constructor(texture : PIXI.Texture, x:number, y:number,  width:number, height : number) {
      super(texture);
      this.position.x = x;
      this.position.y = y;
      this.width = width;
      this.height = height;
      this.velocity = 0.10;
      this.aceleration = 1.1;	
      this.friction = 0.96;	
      this.speedLimit = 5;		 
  }

  update() {
      this.velocity = Math.min(this.speedLimit, Math.max(this.velocity, -this.speedLimit)); 
      this.velocity *= this.aceleration;
      this.velocity *= this.friction;
      this.position.y-= this.velocity;
  }
}

export class Player extends Sprite implements Entity, Attack {
    keyState: object;
    keyCodes: object;
    shoot:boolean;
    projectils : Rocket[][];
    directionX : number;
    directionY : number; 
    accelerationX : number;
    accelerationY : number;
    velocityX : number;
    velocityY : number;
    friction : number;
    speed : number;
    fireSpeed : number;
    speedLimit : number;
    fireCooldown : number;
    widthRocket: number;
    heightRocket:number;
    buildRocket: ExperimentProjectilFactory;
    missle_sound: object;
    constructor(texture : PIXI.Texture, textureRocket : PIXI.Texture, x:number, y:number,  width:number, height:number) {
        super(texture)
        this.position.x = x;
        this.position.y = y;
        this.width = width;
        this.height = height;
        this.keyState = {32: false, 37: false, 38: false, 39: false, 40: false};
        this.keyCodes = {37: -1, 38: -1, 39: 1, 40: 1};
        this.directionX = 0;
        this.directionY = 0; 
        this.accelerationX = 0;
        this.accelerationY = 0;
        this.velocityX = 0;
        this.velocityY = 0;
        this.friction = 0.96;
        this.speed = 10;
        this.fireSpeed = 40;
        this.speedLimit = 5;
        this.fireCooldown = 0;
        this.projectils = [[], []];
        this.buildRocket = new ExperimentProjectilFactory(textureRocket);
        this.widthRocket = 20;
        this.heightRocket = 44;
        window.addEventListener('keydown', this.onKeyDown.bind(this));
        window.addEventListener('keyup', this.onKeyUp.bind(this));
    }

    update() {
                           
          this.velocityX += this.directionX * this.accelerationX; 
		  this.velocityY += this.directionY * this.accelerationY;
		  
		  //Apply friction
		  this.velocityX *= this.friction; 
		  this.velocityY *= this.friction;
		  
		  
		  this.velocityX = Math.min(this.speedLimit, Math.max(this.velocityX, -this.speedLimit));
		  this.velocityY = Math.min(this.speedLimit, Math.max(this.velocityY, -this.speedLimit));
		  
          if ((this.y + this.height) > HEIGHT)
		  {
            this.y = HEIGHT - this.height;

          } else if(this.x < 0 - this.width) {
            this.x = WIDTH - this.width;

          } else if((this.x + this.width/2) > WIDTH) {
            this.x = 0 + this.width;
          
          } else if((this.y - this.height) < (HEIGHT / 3)) {
            this.y = HEIGHT / 3 + this.height;
            
          } else { 
            this.x += this.velocityX;
            this.y += this.velocityY;
          }
          
        this.updateFire();   
    }

    updateFire() {
        if(this.shoot){
            if (this.fireCooldown < this.fireSpeed)
            this.fireCooldown++;
        }

        if(this.fireCooldown >= this.fireSpeed){
            this.projectils[0].push(this.buildRocket.buildRocket(this.position.x, this.position.y, this.widthRocket, this.heightRocket));
            
            this.projectils[1].push(this.buildRocket.buildRocket(this.position.x + this.width - 20, this.position.y, this.widthRocket, this.heightRocket));
            
            this.fireCooldown = 0;
            this.shoot = false;
        }
    }

    onKeyDown(key:KeyboardEvent){
        this.keyState[key.keyCode] = true;
                    
        if (key.keyCode == 37 || key.keyCode == 39){
            this.directionX = this.keyCodes[key.keyCode];
            this.accelerationX = 0.3;
        
        }else if (key.keyCode == 38 || key.keyCode == 40){
            this.directionY = this.keyCodes[key.keyCode]; 
            this.accelerationY = 0.3;
        } else if(key.keyCode == 32) {
            this.shoot = true;
        } 
    }

    onKeyUp(key:KeyboardEvent) {
        this.keyState[key.keyCode] = false;  
                if (!this.keyState[37] && this.keyState[39])
                    this.directionX = this.keyCodes[39];
                else if (this.keyState[37] && !this.keyState[39])
                    this.directionX = this.keyCodes[37];
                else this.directionX = 0,  this.accelerationX = 0, this.friction = 0.96;

                if (!this.keyState[38] && this.keyState[40])
                    this.directionY = this.keyCodes[40];
                else if (this.keyState[38] && !this.keyState[40])
                    this.directionY = this.keyCodes[38];
                else this.directionY = 0, this.accelerationY = 0, this.friction = 0.96;
    }
}




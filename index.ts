import {remote} from 'electron';

import PIXI = require('pixi.js');

import {Background} from './background';
import {Explosion} from './explosion';
import {Meteor} from './entities';
import {Alien, Laser} from './entities';
import {Player, Rocket} from './entities';
import {configureExplosion, WIDTH, HEIGHT, TypeEntity} from './constantes';

let app = new PIXI.Application({width:WIDTH, height:HEIGHT})
document.body.appendChild(app.view);
const loader = new PIXI.Loader();
let player = null, background = null, meteors = [], explosion = null, 
recicleExplosion : Explosion[]  = [], enemies = [];
var TextureCache = PIXI.utils.TextureCache;
var enemyProjectils = [];
let score : Score;
let pontos = 0;
let live = true;
let timeDie = 0;
let beginDie = false;
var textureTitle:PIXI.Texture;
var timeInitMeteor = 250,  timeInitEnemies = 860, timeGameOver = 0; 

// enum TypeEntity {
//     PLAYER, ENEMY, BACKGROUD, ROCKET, METEOR, EXPLOSION
// }

interface EntityAttacking {
    attacking() : void;
}

class PlayerAttacking implements EntityAttacking {
    attacking() {
        for(let i = 0; i < player.projectils.length; i++) {
            player.projectils[i].forEach((projectil:Rocket) =>{
                app.stage.addChild(projectil);
                projectil.update();
        
                if(Math.abs(projectil.y) < 0) {
                    app.stage.removeChild(projectil);
                    player.projectils[i].splice(player.projectils[i].indexOf(projectil), 1);
                }
            });
        }
    }
}

class EnemyAttacking implements EntityAttacking {
    attacking() { 
        enemyProjectils.forEach((projectil:Laser) => {
            app.stage.addChild(projectil); 
            projectil.update();  
            
            if(Math.abs(projectil.y) > 800){
                app.stage.removeChild(projectil);
                enemyProjectils.splice(enemyProjectils.indexOf(projectil),1);
            }
        });
    }
}

class EstrategyAttack {
    private static map:Map<TypeEntity, EntityAttacking> = new Map([
        [TypeEntity.PLAYER, new PlayerAttacking()],
        [TypeEntity.ENEMY, new EnemyAttacking()],
    ])

    static getTypeAttack(type : TypeEntity) : void {
        this.map.get(type).attacking()
    }
}

class Score {
    private x:number;
    private y:number;
    scoreText:PIXI.Text;
    highScoreText:PIXI.Text;
    highScore : number;
    constructor(x:number, y:number) {
        this.x = x;
        this.y = y;
        this.highScore = 0;
    }

    result = () : void => {  
      app.stage.removeChild(this.scoreText);
      this.scoreText = new PIXI.Text(`Score: ${pontos}`, {font:"120px Arial", fill:0xF8F8FF})
      this.scoreText.x = this.x;
      this.scoreText.y = this.y;
      app.stage.addChild(this.scoreText);
    }

    resultHighScore = () : void => {
        this.highScore = window.localStorage.getItem("highScore") == undefined ? 0 : Number(window.localStorage.getItem("highScore"));
        app.stage.removeChild(this.highScoreText);
        this.highScoreText = new PIXI.Text(`HighScore: ${this.highScore}`, {font:"120px Arial", fill:0xF8F8FF})
        this.highScoreText.x = WIDTH - 185;
        this.highScoreText.y = 10;
        app.stage.addChild(this.highScoreText);
      }

    save = () : void => {
        if(pontos > this.highScore) {
            localStorage.setItem("highScore", `${pontos}`);
        }
    }
}

const createMeteors = () => {
    let texture = loader.resources["images/meteoro2.png"].texture;
    
    timeInitMeteor--; 
    
    if(timeInitMeteor == 0) {
        for(let i = 0; i< 5 * Math.random(); i++){	
            meteors.push(new Meteor(texture, WIDTH * Math.random(),-10,50,40));
        }
        timeInitMeteor = 250;
    }

    meteors.forEach(meteor => {
        app.stage.addChild(meteor); 
        meteor.update();

        if(Math.abs(meteor.y) > 600){
            app.stage.removeChild(meteor);
            meteors.splice(meteors.indexOf(meteor),1);
        }
    });
}

const createEnemies = () => {
    let alienTextures = new Array(
        loader.resources["images/alien.png"].texture,
        loader.resources["images/alien2.png"].texture);
        let textureLaser = loader.resources["images/laser.png"].texture;

        timeInitEnemies--;

        if(timeInitEnemies == 0) {
            for(let i = 0;i<= 4;i++){
                let index = Math.round(Math.random() * (alienTextures.length-1));
                let texture = alienTextures[index];
                enemies.push(new Alien(texture,textureLaser, Math.random()*600, -12, 55,32));
            }
            timeInitEnemies = 860;
        }

        enemies.forEach(alien => {
            app.stage.addChild(alien); 
            alien.update();
            alien.updateFire();
            alien.projectils.forEach((projectil:Laser) => {
                enemyProjectils.push(projectil);     
            }); 

            if(Math.abs(alien.y) > 600){
                enemies.splice(enemies.indexOf(alien), 1);
            }
        });
}

const explodir = (x:number, y:number, loop: boolean) => {
    let explosion = new Explosion(configureExplosion(TextureCache["images/Explosion2.png"]), x, y, loop);
    explosion.animationSpeed = 0.1;
    explosion.loop = false;
    app.stage.addChild(explosion); 
    explosion.play();
    explosion.playSound();
    recicleExplosion.push(explosion);
    
}

function collisionRocket<E extends Alien | Meteor> (projectils:Rocket[][], anyEntities: E[]) {
    for(let i = 0; i < player.projectils.length; i++) {
        projectils[i].forEach((projectil : Rocket) => {
            anyEntities.forEach(e => {
                if(projectil != undefined) {
                    if(!(projectil.x + projectil.width < e.x) &&
                                        !(e.x + e.width < projectil.x) &&
                                        !(projectil.y + projectil.height < e.y) &&
                                        !(e.y + e.height < projectil.y)
                                    ) {
                        app.stage.removeChild(e);
                        explodir(e.x - e.width, e.y - e.height, true);
                        pontos++;                    
                        anyEntities.splice(anyEntities.indexOf(e),1);

                        app.stage.removeChild(projectil);
                        projectils[i].splice(projectils[i].indexOf(projectil),1);
                    }
                } 
            });
        });
    }
}

function collisionPlayer<E extends Meteor | Alien | Laser> (anyEntities: E[]) {
    anyEntities.forEach(e => {
        if(e != null && player != null) {
            if(!(e.x + e.width < player.x) &&
                                !(player.x + player.width < e.x) &&
                                !(e.y + e.height < player.y) &&
                                !(player.y + player.height < e.y)
                            ) {
                    
                explodir(player.x - player.width, player.y - player.height, true);
                app.stage.removeChild(e);
                app.stage.removeChild(player);
                player = null;                    
                anyEntities.splice(anyEntities.indexOf(e), 1);
                beginDie = true;
            }
        }
    });
}

const buttonStart = () => {
    let startText = new PIXI.Text(`Start Game`, {font:"220px Arial", fill:0xF8F8FF})
    let texture = PIXI.Texture.from("images/spaceTitulo.png");
    let spriteTitulo = new PIXI.Sprite(texture);
    app.stage.addChild(spriteTitulo);
    app.stage.addChild(startText);
    spriteTitulo.x = 0;
    spriteTitulo.y = 0;
    startText.x = (WIDTH / 2) - 60;
    startText.y = HEIGHT / 2;
    startText.interactive = true;
    startText.buttonMode = true;
    startText.on('pointerdown',() => {
        app.stage.removeChild(startText);
        app.stage.removeChild(spriteTitulo);
        live = true;
        start();
    })
}

const restart = () => {
    app.stage.removeChild(background);

    meteors.forEach((meteor:Meteor) => {
        app.stage.removeChild(meteor);
    });

    removeExplosions();
    meteors = new Array(), 
    recicleExplosion = [],

    enemies.forEach((enemy:Alien) => {
        app.stage.removeChild(enemy);
    });

    enemies = [];

    enemyProjectils.forEach((projectil:Laser) => {
        app.stage.removeChild(projectil);
    });

    background.stopSound();
    app.stage.removeChild(score.scoreText);
    app.stage.removeChild(score.highScoreText);
    score.save();
    enemyProjectils = [];
    pontos = 0;
    timeInitMeteor = 250, 
    timeInitEnemies = 860; 
    timeGameOver = 100;
    buttonStart();
}

loader
.add("images/nave.png")
.add("images/foguete1.png")
.add("images/espaco.png")
.add("images/meteoro2.png")
.add("images/Explosion2.png")
.add("images/alien.png")
.add("images/alien2.png")
.add("images/laser.png")
.add("images/spaceTitulo.png")
.add("fonts/MCKLST__.ttf");

const start = () => {
    loader.load(() => {  
        let textureShip:PIXI.Texture = loader.resources["images/nave.png"].texture;
        let textureRocket:PIXI.Texture = loader.resources["images/foguete1.png"].texture;
        let textureSpace:PIXI.Texture = loader.resources["images/espaco.png"].texture;
        player = new Player(textureShip, textureRocket, (WIDTH/2 - 15), (HEIGHT - 100), 75, 60);
        score = new Score(20, 10);
        background = new Background(textureSpace, WIDTH, HEIGHT);
        background.playSound();
        app.stage.addChild(background);
        app.stage.addChild(player);
        gameLoop();
    });
}

const removeExplosions = () => {
    recicleExplosion.forEach(explosion => {
        if(!explosion.playing) {
            app.stage.removeChild(explosion);    
        }
    });
}

buttonStart();

 function gameLoop() {
     if(beginDie) {
        timeDie++;

        if(timeDie == 160) {
            live = false;
        }
     }

      if(!live) {
            timeGameOver++; 
            let gameOverText = new PIXI.Text(`GAME OVER`, {font:"220px Arial", fill:0xF8F8FF})
            app.stage.addChild(gameOverText);
            gameOverText.x = (WIDTH / 2) - 60;
            gameOverText.y = HEIGHT / 2;
            if(timeGameOver == 100) {
                app.stage.removeChild(gameOverText);
                beginDie = false;
                restart();
                return;
            }       
      }
      
      removeExplosions();

      if(live && player != null) {
            player.update();
            score.result();
            score.resultHighScore();
            background.setViewportY();  
            EstrategyAttack.getTypeAttack(TypeEntity.PLAYER);
            EstrategyAttack.getTypeAttack(TypeEntity.ENEMY);
            createMeteors();
            createEnemies();
      
            collisionRocket<Meteor> (player.projectils, meteors);
            collisionRocket<Alien> (player.projectils, enemies);  
            collisionPlayer<Meteor> (meteors);
            collisionPlayer<Alien> (enemies);
            collisionPlayer<Laser> (enemyProjectils);
      }

    var f = requestAnimationFrame(gameLoop);
 }
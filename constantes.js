"use strict";
exports.__esModule = true;
exports.configureExplosion = exports.TypeEntity = exports.HEIGHT = exports.WIDTH = void 0;
var PIXI = require("pixi.js");
var loader = new PIXI.Loader();
var Texture = PIXI.Texture;
var Rectangle = PIXI.Rectangle;
exports.WIDTH = 600, exports.HEIGHT = 360;
var TypeEntity;
(function (TypeEntity) {
    TypeEntity[TypeEntity["PLAYER"] = 0] = "PLAYER";
    TypeEntity[TypeEntity["ENEMY"] = 1] = "ENEMY";
    TypeEntity[TypeEntity["BACKGROUD"] = 2] = "BACKGROUD";
    TypeEntity[TypeEntity["ROCKET"] = 3] = "ROCKET";
    TypeEntity[TypeEntity["METEOR"] = 4] = "METEOR";
    TypeEntity[TypeEntity["EXPLOSION"] = 5] = "EXPLOSION";
})(TypeEntity = exports.TypeEntity || (exports.TypeEntity = {}));
exports.configureExplosion = function (sheet) {
    var matrix = [
        new Texture(sheet, new Rectangle(0, 0, 128, 128)),
        new Texture(sheet, new Rectangle(128, 0, 128, 128)),
        new Texture(sheet, new Rectangle(256, 0, 128, 128)),
        new Texture(sheet, new Rectangle(384, 0, 128, 128)),
        new Texture(sheet, new Rectangle(0, 128, 128, 128)),
        new Texture(sheet, new Rectangle(128, 128, 128, 128)),
        new Texture(sheet, new Rectangle(256, 128, 128, 128)),
        new Texture(sheet, new Rectangle(384, 128, 128, 128)),
        new Texture(sheet, new Rectangle(0, 256, 128, 128)),
        new Texture(sheet, new Rectangle(128, 256, 128, 128)),
        new Texture(sheet, new Rectangle(256, 256, 128, 128)),
        new Texture(sheet, new Rectangle(384, 256, 128, 128)),
        new Texture(sheet, new Rectangle(0, 384, 128, 128)),
        new Texture(sheet, new Rectangle(128, 384, 128, 128)),
        new Texture(sheet, new Rectangle(256, 384, 128, 128)),
        new Texture(sheet, new Rectangle(384, 384, 128, 128))
    ];
    return matrix;
};

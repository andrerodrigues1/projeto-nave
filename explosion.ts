import {remote} from 'electron';

import PIXI = require('pixi.js');

var AnimatedSprite = PIXI.AnimatedSprite;

export class Explosion extends AnimatedSprite {
    time : number;
    explosionSound : HTMLAudioElement;
    constructor(textures : PIXI.Texture[] , x : number, y : number, loop : boolean) {
        super(textures, loop);
        this.position.x = x;
        this.position.y = y;
        this.time = 0;

        this.explosionSound = document.createElement("audio");

        document.body.appendChild(this.explosionSound);
        this.explosionSound.setAttribute("src","sounds/explosion.wav");
    }

    playSound = () => {
        this.explosionSound.play();    
    }

    startTime = () => {
        this.time+=1;
    }
}
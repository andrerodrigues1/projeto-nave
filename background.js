"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Background = void 0;
var PIXI = require("pixi.js");
var TilingSprite = PIXI.TilingSprite;
var Background = /** @class */ (function (_super) {
    __extends(Background, _super);
    function Background(texture, width, height, x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        var _this = _super.call(this, texture, width, height) || this;
        _this.playSound = function () {
            _this.backgroundSound.play();
            _this.backgroundSound.loop = true;
        };
        _this.stopSound = function () {
            _this.backgroundSound.pause();
        };
        _this.position.x = x;
        _this.position.y = y;
        _this.tilePosition.x = 0;
        _this.tilePosition.y = 0;
        _this.viewportY = 0;
        _this.DELTA_Y = 0.828;
        _this.backgroundSound = document.createElement("audio");
        document.body.appendChild(_this.backgroundSound);
        _this.backgroundSound.setAttribute("src", "sounds/darkfactory.mp3");
        return _this;
    }
    Background.prototype.setViewportY = function () {
        this.tilePosition.y += this.DELTA_Y;
    };
    return Background;
}(TilingSprite));
exports.Background = Background;

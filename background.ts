import {remote} from 'electron';

import PIXI = require('pixi.js');

var TilingSprite = PIXI.TilingSprite;

export class Background extends TilingSprite {
    viewportY : number;
    DELTA_Y : number;
    backgroundSound: HTMLAudioElement;
    constructor(texture : PIXI.Texture, width : number, height : number, x = 0, y = 0) {
        super(texture, width, height);
        this.position.x = x;
        this.position.y = y;
        this.tilePosition.x = 0;
        this.tilePosition.y = 0;
        this.viewportY = 0;
        this.DELTA_Y = 0.828;
        this.backgroundSound = document.createElement("audio");
		document.body.appendChild(this.backgroundSound);
		this.backgroundSound.setAttribute("src","sounds/darkfactory.mp3");
    }
    
    playSound = () => {
        this.backgroundSound.play();
        this.backgroundSound.loop = true;
    }

    stopSound = () => {
        this.backgroundSound.pause();
    }

    setViewportY() {
        this.tilePosition.y += this.DELTA_Y;
    }
}
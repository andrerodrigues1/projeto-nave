"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Explosion = void 0;
var PIXI = require("pixi.js");
var AnimatedSprite = PIXI.AnimatedSprite;
var Explosion = /** @class */ (function (_super) {
    __extends(Explosion, _super);
    function Explosion(textures, x, y, loop) {
        var _this = _super.call(this, textures, loop) || this;
        _this.playSound = function () {
            _this.explosionSound.play();
        };
        _this.startTime = function () {
            _this.time += 1;
        };
        _this.position.x = x;
        _this.position.y = y;
        _this.time = 0;
        _this.explosionSound = document.createElement("audio");
        document.body.appendChild(_this.explosionSound);
        _this.explosionSound.setAttribute("src", "sounds/explosion.wav");
        return _this;
    }
    return Explosion;
}(AnimatedSprite));
exports.Explosion = Explosion;

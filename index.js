"use strict";
exports.__esModule = true;
var PIXI = require("pixi.js");
var background_1 = require("./background");
var explosion_1 = require("./explosion");
var entities_1 = require("./entities");
var entities_2 = require("./entities");
var entities_3 = require("./entities");
var constantes_1 = require("./constantes");
var app = new PIXI.Application({ width: constantes_1.WIDTH, height: constantes_1.HEIGHT });
document.body.appendChild(app.view);
var loader = new PIXI.Loader();
var player = null, background = null, meteors = [], explosion = null, recicleExplosion = [], enemies = [];
var TextureCache = PIXI.utils.TextureCache;
var enemyProjectils = [];
var score;
var pontos = 0;
var live = true;
var timeDie = 0;
var beginDie = false;
var textureTitle;
var timeInitMeteor = 250, timeInitEnemies = 860, timeGameOver = 0;
var PlayerAttacking = /** @class */ (function () {
    function PlayerAttacking() {
    }
    PlayerAttacking.prototype.attacking = function () {
        var _loop_1 = function (i) {
            player.projectils[i].forEach(function (projectil) {
                app.stage.addChild(projectil);
                projectil.update();
                if (Math.abs(projectil.y) < 0) {
                    app.stage.removeChild(projectil);
                    player.projectils[i].splice(player.projectils[i].indexOf(projectil), 1);
                }
            });
        };
        for (var i = 0; i < player.projectils.length; i++) {
            _loop_1(i);
        }
    };
    return PlayerAttacking;
}());
var EnemyAttacking = /** @class */ (function () {
    function EnemyAttacking() {
    }
    EnemyAttacking.prototype.attacking = function () {
        enemyProjectils.forEach(function (projectil) {
            app.stage.addChild(projectil);
            projectil.update();
            if (Math.abs(projectil.y) > 800) {
                app.stage.removeChild(projectil);
                enemyProjectils.splice(enemyProjectils.indexOf(projectil), 1);
            }
        });
    };
    return EnemyAttacking;
}());
var EstrategyAttack = /** @class */ (function () {
    function EstrategyAttack() {
    }
    EstrategyAttack.getTypeAttack = function (type) {
        this.map.get(type).attacking();
    };
    EstrategyAttack.map = new Map([
        [constantes_1.TypeEntity.PLAYER, new PlayerAttacking()],
        [constantes_1.TypeEntity.ENEMY, new EnemyAttacking()],
    ]);
    return EstrategyAttack;
}());
var Score = /** @class */ (function () {
    function Score(x, y) {
        var _this = this;
        this.result = function () {
            app.stage.removeChild(_this.scoreText);
            _this.scoreText = new PIXI.Text("Score: " + pontos, { font: "120px Arial", fill: 0xF8F8FF });
            _this.scoreText.x = _this.x;
            _this.scoreText.y = _this.y;
            app.stage.addChild(_this.scoreText);
        };
        this.resultHighScore = function () {
            _this.highScore = window.localStorage.getItem("highScore") == undefined ? 0 : Number(window.localStorage.getItem("highScore"));
            app.stage.removeChild(_this.highScoreText);
            _this.highScoreText = new PIXI.Text("HighScore: " + _this.highScore, { font: "120px Arial", fill: 0xF8F8FF });
            _this.highScoreText.x = constantes_1.WIDTH - 185;
            _this.highScoreText.y = 10;
            app.stage.addChild(_this.highScoreText);
        };
        this.save = function () {
            if (pontos > _this.highScore) {
                localStorage.setItem("highScore", "" + pontos);
            }
        };
        this.x = x;
        this.y = y;
        this.highScore = 0;
    }
    return Score;
}());
var createMeteors = function () {
    var texture = loader.resources["images/meteoro2.png"].texture;
    timeInitMeteor--;
    if (timeInitMeteor == 0) {
        for (var i = 0; i < 5 * Math.random(); i++) {
            meteors.push(new entities_1.Meteor(texture, constantes_1.WIDTH * Math.random(), -10, 50, 40));
        }
        timeInitMeteor = 250;
    }
    meteors.forEach(function (meteor) {
        app.stage.addChild(meteor);
        meteor.update();
        if (Math.abs(meteor.y) > 600) {
            app.stage.removeChild(meteor);
            meteors.splice(meteors.indexOf(meteor), 1);
        }
    });
};
var createEnemies = function () {
    var alienTextures = new Array(loader.resources["images/alien.png"].texture, loader.resources["images/alien2.png"].texture);
    var textureLaser = loader.resources["images/laser.png"].texture;
    timeInitEnemies--;
    if (timeInitEnemies == 0) {
        for (var i = 0; i <= 4; i++) {
            var index = Math.round(Math.random() * (alienTextures.length - 1));
            var texture = alienTextures[index];
            enemies.push(new entities_2.Alien(texture, textureLaser, Math.random() * 600, -12, 55, 32));
        }
        timeInitEnemies = 860;
    }
    enemies.forEach(function (alien) {
        app.stage.addChild(alien);
        alien.update();
        alien.updateFire();
        alien.projectils.forEach(function (projectil) {
            enemyProjectils.push(projectil);
        });
        if (Math.abs(alien.y) > 600) {
            enemies.splice(enemies.indexOf(alien), 1);
        }
    });
};
var explodir = function (x, y, loop) {
    var explosion = new explosion_1.Explosion(constantes_1.configureExplosion(TextureCache["images/Explosion2.png"]), x, y, loop);
    explosion.animationSpeed = 0.1;
    explosion.loop = false;
    app.stage.addChild(explosion);
    explosion.play();
    explosion.playSound();
    recicleExplosion.push(explosion);
};
function collisionRocket(projectils, anyEntities) {
    var _loop_2 = function (i) {
        projectils[i].forEach(function (projectil) {
            anyEntities.forEach(function (e) {
                if (projectil != undefined) {
                    if (!(projectil.x + projectil.width < e.x) &&
                        !(e.x + e.width < projectil.x) &&
                        !(projectil.y + projectil.height < e.y) &&
                        !(e.y + e.height < projectil.y)) {
                        app.stage.removeChild(e);
                        explodir(e.x - e.width, e.y - e.height, true);
                        pontos++;
                        anyEntities.splice(anyEntities.indexOf(e), 1);
                        app.stage.removeChild(projectil);
                        projectils[i].splice(projectils[i].indexOf(projectil), 1);
                    }
                }
            });
        });
    };
    for (var i = 0; i < player.projectils.length; i++) {
        _loop_2(i);
    }
}
function collisionPlayer(anyEntities) {
    anyEntities.forEach(function (e) {
        if (e != null && player != null) {
            if (!(e.x + e.width < player.x) &&
                !(player.x + player.width < e.x) &&
                !(e.y + e.height < player.y) &&
                !(player.y + player.height < e.y)) {
                explodir(player.x - player.width, player.y - player.height, true);
                app.stage.removeChild(e);
                app.stage.removeChild(player);
                player = null;
                anyEntities.splice(anyEntities.indexOf(e), 1);
                beginDie = true;
            }
        }
    });
}
var buttonStart = function () {
    var startText = new PIXI.Text("Start Game", { font: "220px Arial", fill: 0xF8F8FF });
    var texture = PIXI.Texture.from("images/spaceTitulo.png");
    var spriteTitulo = new PIXI.Sprite(texture);
    app.stage.addChild(spriteTitulo);
    app.stage.addChild(startText);
    spriteTitulo.x = 0;
    spriteTitulo.y = 0;
    startText.x = (constantes_1.WIDTH / 2) - 60;
    startText.y = constantes_1.HEIGHT / 2;
    startText.interactive = true;
    startText.buttonMode = true;
    startText.on('pointerdown', function () {
        app.stage.removeChild(startText);
        app.stage.removeChild(spriteTitulo);
        live = true;
        start();
    });
};
var restart = function () {
    app.stage.removeChild(background);
    meteors.forEach(function (meteor) {
        app.stage.removeChild(meteor);
    });
    removeExplosions();
    meteors = new Array(),
        recicleExplosion = [],
        enemies.forEach(function (enemy) {
            app.stage.removeChild(enemy);
        });
    enemies = [];
    enemyProjectils.forEach(function (projectil) {
        app.stage.removeChild(projectil);
    });
    background.stopSound();
    app.stage.removeChild(score.scoreText);
    app.stage.removeChild(score.highScoreText);
    score.save();
    enemyProjectils = [];
    pontos = 0;
    timeInitMeteor = 250,
        timeInitEnemies = 860;
    timeGameOver = 100;
    buttonStart();
};
loader
    .add("images/nave.png")
    .add("images/foguete1.png")
    .add("images/espaco.png")
    .add("images/meteoro2.png")
    .add("images/Explosion2.png")
    .add("images/alien.png")
    .add("images/alien2.png")
    .add("images/laser.png")
    .add("images/spaceTitulo.png")
    .add("fonts/MCKLST__.ttf");
var start = function () {
    loader.load(function () {
        var textureShip = loader.resources["images/nave.png"].texture;
        var textureRocket = loader.resources["images/foguete1.png"].texture;
        var textureSpace = loader.resources["images/espaco.png"].texture;
        player = new entities_3.Player(textureShip, textureRocket, (constantes_1.WIDTH / 2 - 15), (constantes_1.HEIGHT - 100), 75, 60);
        score = new Score(20, 10);
        background = new background_1.Background(textureSpace, constantes_1.WIDTH, constantes_1.HEIGHT);
        background.playSound();
        app.stage.addChild(background);
        app.stage.addChild(player);
        gameLoop();
    });
};
var removeExplosions = function () {
    recicleExplosion.forEach(function (explosion) {
        if (!explosion.playing) {
            app.stage.removeChild(explosion);
        }
    });
};
buttonStart();
function gameLoop() {
    if (beginDie) {
        timeDie++;
        if (timeDie == 160) {
            live = false;
        }
    }
    if (!live) {
        timeGameOver++;
        var gameOverText = new PIXI.Text("GAME OVER", { font: "220px Arial", fill: 0xF8F8FF });
        app.stage.addChild(gameOverText);
        gameOverText.x = (constantes_1.WIDTH / 2) - 60;
        gameOverText.y = constantes_1.HEIGHT / 2;
        if (timeGameOver == 100) {
            app.stage.removeChild(gameOverText);
            beginDie = false;
            restart();
            return;
        }
    }
    removeExplosions();
    if (live && player != null) {
        player.update();
        score.result();
        score.resultHighScore();
        background.setViewportY();
        EstrategyAttack.getTypeAttack(constantes_1.TypeEntity.PLAYER);
        EstrategyAttack.getTypeAttack(constantes_1.TypeEntity.ENEMY);
        createMeteors();
        createEnemies();
        collisionRocket(player.projectils, meteors);
        collisionRocket(player.projectils, enemies);
        collisionPlayer(meteors);
        collisionPlayer(enemies);
        collisionPlayer(enemyProjectils);
    }
    var f = requestAnimationFrame(gameLoop);
}
